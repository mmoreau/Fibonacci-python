# Fibonacci

<h3>What is the Fibonacci suite ?</h3>
<ul>
    <li>https://en.wikipedia.org/wiki/Fibonacci_number</li>
</ul>

<h2>Script</h2>
<p>Fibonacci.Fibonacci(N)<p>
<ul>
    <li>N : The number of the Fibonacci suite to be calculated</li>
</ul>

<h3>Example</h3>
<p>Display the 10th suite of Fiboanacci</p>
<pre>
    <code>
        print(Fibonacci.Fibonacci(10))
    </code>
</pre>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>