class Fibonacci:

	@classmethod
	def Calc(cls, n):
            
		if isinstance(n, int):
			if n >= 0:
				
				a, b = 0, 1

				for i in range(n):
					a, b = b, a + b

				return a
		return None
